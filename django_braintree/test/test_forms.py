"""
tests for the forms for the payment system.
"""
from unittest import mock

from django.test import TestCase

from django_braintree import forms
from django_braintree.models import BrainTreePayment
from django_braintree.test import factories


class PaymentFormTestCase(TestCase):

    form = forms.PaymentForm

    def setUp(self):
        self.user = factories.UserFactory()
        self.payment = factories.BrainTreePaymentFactory(user=self.user)
        self.data = {
            'amount': '5.00',
            'payment': self.payment.pk,
            'nonce': 'test_nonce'
        }

        self.patched_sale = mock.patch('braintree.Transaction.sale')
        self.mock_sale = self.patched_sale.start()
        self.addCleanup(self.patched_sale.stop)

        self.fail_mock = mock.MagicMock()
        self.fail_mock.is_success = False
        self.fail_mock.errors = 'Error'

    def test_successful_payment(self):
        """ Ensure a successful payment works. """
        mock_return = mock.MagicMock()
        mock_return.is_success = True
        self.mock_sale.return_value = mock_return

        form = self.form(self.user, self.data)
        self.assertTrue(form.is_valid())
        res = form.execute()

        self.assertIsInstance(res, BrainTreePayment)
        self.assertTrue(res.success)

    def test_invalid_on_not_user(self):
        """ Ensure form fails validation if user doesn't match payment user. """
        payment = factories.BrainTreePaymentFactory()
        self.data['payment'] = payment.pk

        form = self.form(self.user, self.data)
        self.assertFalse(form.is_valid())

    def test_invalid_on_field_mismatch(self):
        """ Ensure form fails validation if user given inputs don't match expected. """
        self.data['amount'] = '6.00'

        form = self.form(self.user, self.data)
        self.assertFalse(form.is_valid())
