"""
Tests for all views related to payments.
"""
from unittest import mock

from django.test import TestCase
from rest_framework.test import APIClient

from django_braintree.models import BrainTreePayment
from django_braintree.test import factories


class PaymentsViewSetTestCase(TestCase):
    """ Test for th payments view set. """
    def setUp(self):
        self.client = APIClient()
        self.user = factories.UserFactory()
        self.client.force_authenticate(user=self.user)

    @mock.patch('braintree.ClientToken.generate')
    def test_creates_payment(self, mock_token):
        """ Ensure we create a payment on a successful post. """
        mock_token.return_value = 'test_token'
        data = {
            'amount': 5.00
        }
        response = self.client.post('/payments/', data).json()

        assert 1 == BrainTreePayment.objects.count()
        payment = BrainTreePayment.objects.all()[0]
        assert response['payment']['id'] == payment.id
        assert response['authorization'] == 'test_token'

    def test_failed_create_gives_400(self):
        """ Ensure we get a 400 on a failed post """
        response = self.client.post('/payments/', {})

        self.assertEqual(response.status_code, 400)

    @mock.patch('braintree.Transaction.sale')
    def test_pay_payment(self, mock_sale):
        """ Ensure paying a payment successfully returns the payment. """
        mock_return = mock.MagicMock()
        mock_return.is_success = True
        mock_sale.return_value = mock_return

        payment = factories.BrainTreePaymentFactory(user=self.user)

        data = {
            'amount': payment.amount,
            'nonce': 'test_nonce'
        }

        response = self.client.post('/payments/{}/pay/'.format(payment.pk), data)
        self.assertEqual(response.status_code, 200)

        payment.refresh_from_db()
        self.assertTrue(payment.success)

    @mock.patch('braintree.Transaction.sale')
    def test_403_on_payment_error(self, mock_sale):
        """ Ensure a failed payment raises a 403. """
        mock_return = mock.MagicMock()
        mock_return.is_success = False
        mock_return.errors = 'Error'
        mock_sale.return_value = mock_return

        payment = factories.BrainTreePaymentFactory(user=self.user)

        data = {
            'amount': payment.amount,
            'nonce': 'test_nonce'
        }

        response = self.client.post('/payments/{}/pay/'.format(payment.pk), data)
        self.assertEqual(response.status_code, 403)
        self.assertDictEqual({'error': 'Error'}, response.json())

        payment.refresh_from_db()
        self.assertFalse(payment.success)

    @mock.patch('braintree.Transaction.sale')
    def test_400_on_generic_error(self, mock_sale):
        """ Ensure a generic form error raises a 400 on payment. """
        mock_return = mock.MagicMock()
        mock_return.is_success = True
        mock_sale.return_value = mock_return

        payment = factories.BrainTreePaymentFactory(user=self.user)

        data = {
            'amount': payment.amount
        }

        response = self.client.post('/payments/{}/pay/'.format(payment.pk), data)
        self.assertEqual(response.status_code, 400)

        payment.refresh_from_db()
        self.assertFalse(payment.success)

    @mock.patch('braintree.Transaction.sale')
    def test_400_on_wrong_amount_error(self, mock_sale):
        """ Ensure a generic form error raises a 400 on payment. """
        mock_return = mock.MagicMock()
        mock_return.is_success = True
        mock_sale.return_value = mock_return

        payment = factories.BrainTreePaymentFactory(user=self.user)

        data = {
            'amount': '6.00'
        }

        response = self.client.post('/payments/{}/pay/'.format(payment.pk), data)
        self.assertEqual(response.status_code, 400)

        payment.refresh_from_db()
        self.assertFalse(payment.success)
