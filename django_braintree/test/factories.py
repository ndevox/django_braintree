""" Model factories for payments. """
import factory
from django.contrib.auth.models import User

from django_braintree import models


class UserFactory(factory.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.sequence(lambda x: 'TestUser{}'.format(x))
    password = 'test'


class BrainTreePaymentFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.BrainTreePayment

    user = factory.SubFactory(UserFactory)
    amount = '5.00'
