"""
Serializers for the payment system.
"""
from rest_framework import serializers

from django_braintree import models


class BrainTreePaymentSerializer(serializers.ModelSerializer):
    """ Serializer for a generic payment. """
    class Meta:
        model = models.BrainTreePayment

        fields = (
            'amount', 'id'
        )
