from django.conf.global_settings import AUTH_USER_MODEL
from django.db import models


class BrainTreePayment(models.Model):
    """
    Hold historical payment information for users.
    """

    user = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='braintree_payments')

    amount = models.DecimalField(max_digits=10, decimal_places=2)

    payment_date = models.DateTimeField(auto_now_add=True)
    success = models.BooleanField(default=False)
