"""
View handling for the payments system.
"""

import braintree
from rest_framework import viewsets

from rest_framework.decorators import action
from rest_framework.response import Response

from django_braintree import forms, models
from django_braintree.exceptions import FailedPaymentException
from django_braintree.serializers import BrainTreePaymentSerializer


class PaymentViewSet(viewsets.GenericViewSet):
    """ Payments views are grouped into this viewset. """
    model = models.BrainTreePayment

    serializer_class = BrainTreePaymentSerializer

    def client_token(self):
        """ Generate a client token for the BrainTree front-end. """
        return {'authorization': braintree.ClientToken.generate()}

    def create(self, request):
        """ Create a payment for someone to pay. """
        data = (request.POST or request.data).copy()

        data['user'] = request.user.pk

        form = forms.CreatePaymentForm(data)

        if form.is_valid():
            payment_dict = self.client_token()
            payment_dict['payment'] = self.serializer_class(
                form.save(),
                context={
                    'request': request
                },
                many=False
            ).data

            return Response(payment_dict, status=201)

        return Response(form.errors, status=400)

    @action(detail=True, methods=['POST'])
    def pay(self, request, pk):
        """ Pay off a payment. """
        data = (request.POST or request.data).copy()

        data['payment'] = pk

        form = forms.PaymentForm(request.user, data)

        if form.is_valid():
            try:
                data = self.serializer_class(
                    form.execute(),
                    context={
                        'request': request
                    }
                ).data
                return Response(data)
            except FailedPaymentException as err:
                return Response({'error': str(err)}, status=403)
        return Response(form.errors, status=400)
