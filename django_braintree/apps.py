from django.apps import AppConfig


class DjangoBraintreeConfig(AppConfig):
    name = 'django_braintree'
