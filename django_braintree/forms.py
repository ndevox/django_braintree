import braintree
from django import forms
from django.core.exceptions import ValidationError

from django_braintree.exceptions import FailedPaymentException
from django_braintree import models


class PaymentForm(forms.Form):
    """
    Simple payment form to accept payment details and a payment_method_nonce.

    Sends a sale to the BrainTree server and checks the result.

    If succeeded it marks the payment a success. Otherwise it returns the errors.
    """

    nonce = forms.CharField()
    amount = forms.DecimalField(max_digits=20, decimal_places=2)
    payment = forms.ModelChoiceField(models.BrainTreePayment.objects.all())

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user
        self.fields['payment'].queryset = user.braintree_payments.all()

    def execute(self):
        """
        Run the payment. If it fails, raise an error.

        If it succeeds, return the payment.
        """
        payment = self.cleaned_data['payment']

        result = braintree.Transaction.sale({
            'amount': str(payment.amount),
            'payment_method_nonce': self.cleaned_data['nonce'],
            'options': {
                'submit_for_settlement': True
            }
        })

        if result.is_success:
            payment.success = True
            payment.save()
            return payment

        raise FailedPaymentException(result.errors)

    def clean(self):
        cleaned_data = super(PaymentForm, self).clean()

        if self.errors:
            return cleaned_data

        if self.cleaned_data['payment'].amount != cleaned_data['amount']:
            raise ValidationError('Fields mismatch on payment.')

        return cleaned_data


class CreatePaymentForm(forms.ModelForm):
    """
    To start the payment process we first need to create a payment.
    """

    class Meta:
        model = models.BrainTreePayment
        fields = '__all__'
