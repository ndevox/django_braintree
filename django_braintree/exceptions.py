""" Exceptions for the payments system. """


class FailedPaymentException(Exception):
    """ Thrown on a failed payment. """
    pass
