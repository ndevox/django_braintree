"""
Routing for the payments system.

This is by and large controlled by Django Rest Frameworks routing system.
"""
from django.conf.urls import url, include

from rest_framework_nested import routers

from django_braintree import views

payment_router = routers.SimpleRouter()
payment_router.register(r'payments', views.PaymentViewSet, base_name='payment')

urlpatterns = [
    url('^', include(payment_router.urls)),
]
