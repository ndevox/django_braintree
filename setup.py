from setuptools import setup, find_packages

install_reqs = [req.strip() for req in open('requirements.txt').readlines()]

setup(
    name="django_braintree",
    version="0.1.0",
    packages=find_packages(),
    install_requires=install_reqs,
    url="https://gitlab.com/ndevox/django_braintree"
)
